# flow3r-cert
## CERT beedoo for flow3r-badge

### Installation
Put your flow3r into disk mode https://docs.flow3r.garden/badge/programming.html#disk-mode
Put the files to sys/apps/sosi/
Press left shoulder-button to exit

### Usage
Go to Badge and use SoSi
put the left shoulder button to left to toggle blue light flashing (siren goes automatically off)
put the left shoulder button to right to toggle siren wailing (only when light activated)
Add team to nick.json ({"name": "flow3r", "team": "visitor", "font": 4, "pronouns": ["she", "DE", "EN"], "pronouns_size": 25, "color": "0x40ff22", "mode": 1, "size": 40})
### Exit
Press both shoulder-buttons

### Changelog
#### Version 8
fixed wave file

#### Version 8
fixed more clean exit

#### Version 7
fixed volume set to max always

#### Version 6
fixed missing wav

#### Version 5
Makes the badge with beedo wailing
Added Pronouns and Team

#### Version 4
Makes the badge with siren wailing

#### Version 3
put the left shoulder button to left to toggle blue light flashing
put the left shoulder button to right to toggle siren wailing (tbd)

#### Version 2
Shows Nickname

#### Version 1
Makes the badge with blue light flashing